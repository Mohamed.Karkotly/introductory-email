# Introductory Email

[![Mohamed Karkotly](https://img.shields.io/badge/Developer-Mohamed%20Karkotly-lightgrey)](https://www.linkedin.com/in/mohamed-karkotly/)

###### _This is a simple responsive introductory email built with MJML._
